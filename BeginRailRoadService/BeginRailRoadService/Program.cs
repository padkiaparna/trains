﻿using RailRoadService.Command;
using RailRoadService.CommuterEnquiry;
using RailRoadService.Graph;
using System;
using System.IO;

namespace BeginRailRoadService
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var commuter = new CommuterQuery(GraphBuilder.GetEmptyGraph());
                Console.WriteLine("Enter 'exit' to quit");
                Console.WriteLine("****Input file path*****");
                string input = Console.ReadLine();
                if (input.Equals("exit", StringComparison.OrdinalIgnoreCase)) break;
                string filePath = "../../input.txt";
                if (File.Exists(input)) filePath = input;
                try
                {
                    string[] inputLines = File.ReadAllLines(filePath);
                    var inputCommands = new CommandBuilder(inputLines).GetCommands();
                    foreach (var command in inputCommands)
                    {
                        command.Execute(commuter);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            
            
        }
    }
}
