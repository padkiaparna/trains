﻿using Moq;
using NUnit.Framework;
using RailRoadService.Command;
using RailRoadService.CommuterEnquiry;
using System;
using System.Collections.Generic;

namespace RailRoadServiceTests.Command
{
    [TestFixture]
    public class DurationCommandTests
    {
        private Mock<ICommuterQuery> _commuter;

        [SetUp]
        public void Setup()
        {
            _commuter = new Mock<ICommuterQuery>();
        }

        [Test]
        public void Execute_WithOneStop_CallsRouteDuration()
        {
            //arrange
            DurationCommand command = new DurationCommand("DURATION: A-B-C");
            List<String> intermediate = new List<String>();
            intermediate.Add("B");

            //act
            command.Execute(_commuter.Object);

            //assert
            _commuter.Verify(c => c.RouteDuration("A", "C", intermediate));
        }

        [Test]
        public void Execute_MultipleIntermediateStops_CallsRouteDuration()
        {
            //arrange
            DurationCommand command = new DurationCommand("DURATION: A-B-C-D-E-F");
            List<String> intermediate = new List<String>();
            intermediate.Add("B");
            intermediate.Add("C");
            intermediate.Add("D");
            intermediate.Add("E");

            //act
            command.Execute(_commuter.Object);

            //assert
            _commuter.Verify(c => c.RouteDuration("A", "F", intermediate));
        }
    }
}
