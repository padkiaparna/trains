﻿using Moq;
using NUnit.Framework;
using RailRoadService.Command;
using RailRoadService.CommuterEnquiry;
using RailRoadService.Graph;

namespace RailRoadServiceTests.Command
{
    [TestFixture]
    public class BuildGraphCommandTests
    {
        private Mock<IGraph<string>> _graph;
        private Mock<ICommuterQuery> _commuter;

        [SetUp]
        public void Setup()
        {
            _commuter = new Mock<ICommuterQuery>();
            _graph = new Mock<IGraph<string>>();

            
        }
        [Test]
        public void Execute_BuildGraph_CallsAppropriateMethods()
        {
            //arrange
            var command = new BuildGraphCommand("Graph: AB3, BC15");
            _commuter.Setup(c => c.GetAllRoutes()).Returns(_graph.Object);

            //act
            command.Execute(_commuter.Object);

            //assert
            _commuter.Verify(c => c.GetAllRoutes(),Times.AtLeastOnce);
            _graph.Verify(g => g.AddNode("A"), Times.Once);
            _graph.Verify(g => g.AddNode("B"), Times.Exactly(2));
            _graph.Verify(g => g.AddNode("C"), Times.Once);
            _graph.Verify(g => g.AddEdge("A", "B", 3));
            _graph.Verify(g => g.AddEdge("B", "C", 15));
        }
    }
}
