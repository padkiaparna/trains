﻿using NUnit.Framework;
using RailRoadService.Graph;
using RailRoadService.Rules;
using System;

namespace RailRoadServiceTests.Rules
{
    [TestFixture]
    public class ContainsPathTests
    {
        private IRule<String> _rule;
        private IPath<String> _finalPath;

        [SetUp]
        public void Setup() {
            _finalPath = GraphPath<String>.EmptyPath();
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("A", "B", 5));
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("B", "C", 15));
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("C", "D", 25));

            _rule = new ContainsPathRule<String>(_finalPath);
        }

        [Test]
        public void IsFulfilled_GivenPathConatinsFinalPath_ReturnsTrue()
        {
            //arrange
            var partialPath = GraphPath<String>.EmptyPath();
            partialPath.AddEdge(Edge<String>.GetWeightedEdge("A", "B", 5));

            //act & assert
            Assert.IsTrue(_rule.IsFulfilled(partialPath));
        }

        [Test]
        public void IsFulfilled_GivenPathDoesNotContainFinalPath_ReturnsFalse()
        {
            //arrange
            var partialPath = GraphPath<String>.EmptyPath();
            partialPath.AddEdge(Edge<String>.GetWeightedEdge("B", "D", 5));

            //act & assert
            Assert.IsFalse(_rule.IsFulfilled(partialPath));
        }

        [Test]
        public void IsFulfilled_BothPathsAreSame_ReturnsTrue()
        {
            //arrange
            var partialPath = GraphPath<String>.CopyPath(_finalPath);

            //act & assert
            Assert.IsTrue(_rule.IsFulfilled(partialPath));
        }
    }
}
