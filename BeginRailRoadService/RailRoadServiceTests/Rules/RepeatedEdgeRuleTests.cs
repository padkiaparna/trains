﻿using NUnit.Framework;
using RailRoadService.Graph;
using RailRoadService.Rules;
using System;

namespace RailRoadServiceTests.Rules
{
    [TestFixture]
    public class RepeatedEdgeRuleTests
    {
        private IRule<String> _rule = new RepeatedEdgeRule<String>();

        [Test]
        public void IsFulfilled_NoRepeatedEdges_ReturnsTrue()
        {
            //arrange
            var finalPath = GraphPath<String>.EmptyPath();
            finalPath.AddEdge(Edge<String>.GetWeightedEdge("A", "B", 5));
            finalPath.AddEdge(Edge<String>.GetWeightedEdge("B", "C", 15));
            finalPath.AddEdge(Edge<String>.GetWeightedEdge("C", "D", 25));

            //act & assert
            Assert.IsTrue(_rule.IsFulfilled(finalPath));
        }

        [Test]
        public void IsFulfilled_RepeatedEdges_ReturnsFalse()
        {
            //arrange
            var finalPath = GraphPath<String>.EmptyPath();
            finalPath.AddEdge(Edge<String>.GetWeightedEdge("A", "B", 5));
            finalPath.AddEdge(Edge<String>.GetWeightedEdge("B", "C", 15));
            finalPath.AddEdge(Edge<String>.GetWeightedEdge("C", "D", 25));
            finalPath.AddEdge(Edge<String>.GetWeightedEdge("D", "A", 15));
            finalPath.AddEdge(Edge<String>.GetWeightedEdge("A", "B", 5));

            //act & assert
            Assert.IsFalse(_rule.IsFulfilled(finalPath));
        }
    }
}
