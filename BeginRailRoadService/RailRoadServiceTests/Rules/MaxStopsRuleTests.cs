﻿using NUnit.Framework;
using RailRoadService.Graph;
using RailRoadService.Rules;
using System;

namespace RailRoadServiceTests.Rules
{
    [TestFixture]
    public class MaxStopsRuleTests
    {
        private IRule<String> _rule = new MaxStopsRule<String>(3);

        [Test]
        public void IsFulFilled_CheckFor3Stops_ReturnsTrue()
        {
            var _finalPath = GraphPath<String>.EmptyPath();
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("A", "B", 5));
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("B", "C", 15));
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("C", "D", 25));
            Assert.IsTrue(_rule.IsFulfilled(_finalPath));
        }

        [Test]
        public void IsFulFilled_Has4Stops_ReturnsFalse()
        {
            IPath<String> _finalPath = GraphPath<String>.EmptyPath();
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("A", "B", 5));
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("B", "C", 15));
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("C", "D", 25));
            _finalPath.AddEdge(Edge<String>.GetWeightedEdge("D", "E", 25));
            Assert.IsFalse(_rule.IsFulfilled(_finalPath));
        }
    }
}
