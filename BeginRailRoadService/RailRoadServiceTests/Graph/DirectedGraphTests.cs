﻿using NUnit.Framework;
using RailRoadService;
using RailRoadService.Graph;
using RailRoadService.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailRoadServiceTests.Graph
{
    [TestFixture]
    public class DirectedGraphTests
    {
        private IGraph<String> graph;

        [SetUp]
        public void Setup()
        {
            graph = GraphBuilder.GetDefaultGraph();
        }

        [Test]
        public void AddNode_NodeIsNull_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => graph.AddNode(null));
        }

        [Test]
        public void AddNode_AddNewNode_ReturnsTrue()
        {
            Assert.IsTrue(graph.AddNode("Z"));
            Assert.IsTrue(graph.GetAllNodes().Contains("Z"));
            Assert.IsFalse(graph.AddNode("Z"));
        }

        [Test]
        public void AddEdge_VertexDoesntExist_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => graph.AddEdge("X", "A", 1));
        }

        [Test]
        public void AddEdge_DuplicateEdgeAdded_ShouldUpdateWeight()
        {
            graph.AddNode("Z");
            Assert.IsTrue(graph.AddEdge("Z", "A", 5));
            Assert.AreEqual(5,graph.GetEdge("Z", "A").GetWeight());

            Assert.IsTrue(graph.AddEdge("Z", "A", 15));
            Assert.AreEqual(15,graph.GetEdge("Z", "A").GetWeight());
        }

        [Test]
        public void GetEdge()
        {
            graph.AddNode("Z");
            graph.AddEdge("A", "Z", 15);
            var azEdge = graph.GetEdge("A", "Z");

            Assert.AreEqual("A",azEdge.GetStartingNode());
            Assert.AreEqual("Z",azEdge.GetEndingNode());
            Assert.AreEqual(15,azEdge.GetWeight());
            Assert.IsNull(graph.GetEdge("Z", "B"));
        }

        [Test]
        public void GetAllNodes()
        {
            var result = graph.GetAllNodes();
            Assert.AreEqual(new List<String>() { "A", "B", "C", "D", "E" }, result.ToList());
        }

        [Test]
        public void GetAllPaths_NoPathExists_ThrowsException()
        {
            Assert.Throws<NoSuchRouteException>(() => graph.GetAllPaths("A", "B", new MaxWeightRule<String>(3)));
        }

        [Test]
        public void GetAllPaths_WithMaxStopsRule_ReturnAppropriatepaths()
        {
            // {a-> d, a->b->c->d, a->d->c->d}
            Assert.AreEqual(3,graph.GetAllPaths("A", "D", new MaxStopsRule<String>(3)).Count);

            // {a->d}
            Assert.AreEqual(1, graph.GetAllPaths("A", "D", new MaxStopsRule<String>(2)).Count);
        }
    }
}
