﻿using NUnit.Framework;
using RailRoadService.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailRoadServiceTests.Graph
{
    [TestFixture]
    public class GraphPathTests
    {
        private IPath<String> path;
        private Edge<String> abEdge = Edge<String>.GetWeightedEdge("A", "B", 5);
        private Edge<String> bcEdge = Edge<String>.GetWeightedEdge("B", "C", 15);
        private Edge<String> cdEdge = Edge<String>.GetWeightedEdge("C", "D", 5);

        [SetUp]
        public void Setup()
        {
            path = GraphPath<String>.EmptyPath();
            path.AddEdge(abEdge);
            path.AddEdge(bcEdge);
            path.AddEdge(cdEdge);
        }

        [Test]
        public void AddEdge_InitializeEdge_IncreaseTotalWeightAndNumberOfStops()
        {
            //arrange
            Edge<String> newEdge = Edge<String>.GetWeightedEdge("D", "E", 5);

            //act
            path.AddEdge(newEdge);

            //Assert
            Assert.AreEqual(30, path.GetPathTotalWeight());
            Assert.AreEqual(4, path.GetNumberOfStops());
        }

        [Test]
        public void AddEdge_EdgeIsNotConsecutive_ThrowsException()
        {
            //arrange
            Edge<String> nonConsecutiveEdge = Edge<String>.GetWeightedEdge("E", "A", 1);

            //act & assert
            Assert.Throws<ArgumentException>(() => path.AddEdge(nonConsecutiveEdge));
        }

        [Test]
        public void GetPathTotalWeight_EdgesSumIs25_ShouldEqualEdgesSum()
        {
            Assert.AreEqual(25, path.GetPathTotalWeight());
        }

        [Test]
        public void GetNumberOfStops_EdgeNumberIs3_ShouldEqualEdgeNumber()
        {
            Assert.AreEqual(3, path.GetNumberOfStops());
        }

        [Test]
        public void GetLastNode_ShouldReturnLastEdgeEndingNode()
        {
            Assert.AreEqual("D", path.GetLastNode());
        }

        [Test]
        public void GetLastNode_PathIsEmpty_ShouldReturnNull()
        {
            path = GraphPath<String>.EmptyPath();
            Assert.IsNull(path.GetLastNode());
        }

        [Test]
        public void RemoveLastEdge_ShouldDecreaseWeightAndStops()
        {
            //act
            path.RemoveLastEdge();

            //assert
            Assert.AreEqual(20, path.GetPathTotalWeight());
            Assert.AreEqual(2, path.GetNumberOfStops());
        }


        //[Test]
        //public void testGetEdgeList()
        //{
        //    var edgeList = path.GetEdgeList();
        //    Assert.Contains(new IEnumerable<Edge<>>{ abEdge, bcEdge, cdEdge},
        //    edgeList);
        //}

        [Test]
        public void HasRepeatedEdges_NoRepeatedEdges_ReturnsFalse()
        {
            Assert.IsFalse(path.HasRepeatedEdges());
        }

        [Test]
        public void HasRepeatedEdges_AddRepeatedEdges_ReturnsTrue()
        {
            //arrange
            Edge<String> daEdge = Edge<String>.GetWeightedEdge("D", "A", 5);
            path.AddEdge(daEdge);
            path.AddEdge(abEdge);

            //act & assert
            Assert.IsTrue(path.HasRepeatedEdges());
        }

        [Test]
        public void CompareTo_SameWeight_Return0()
        {
            var otherPath = GraphPath<String>.CopyPath(path);
            Assert.AreEqual(0, otherPath.CompareTo(path));
        }

        [Test]
        public void CompareTo_OtherPathHasGreaterWeight_ReturnNonZero()
        {
            //arrange
            var otherPath = GraphPath<String>.CopyPath(path);
            var daEdge = Edge<String>.GetWeightedEdge("D", "A", 5);
            otherPath.AddEdge(daEdge);

            //act & assert
            Assert.IsTrue(otherPath.CompareTo(path) > 0);
            Assert.IsTrue(path.CompareTo(otherPath) < 0);
        }
    }
}
