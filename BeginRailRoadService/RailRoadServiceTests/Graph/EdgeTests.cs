﻿using NUnit.Framework;
using RailRoadService.Graph;
using System;

namespace RailRoadServiceTests.Graph
{
    [TestFixture]
    public class EdgeTests
    {
        private Edge<String> edge = Edge<String>.GetWeightedEdge("A", "B", 15);

        [Test]
        public void CompareEdges_SameStartingAndEndingNodes_DifferentWeights_ReturnsTrue()
        {
            Edge<String> edgeWithDifferentWeight = Edge<String>.GetWeightedEdge("A", "B", 5);
            Assert.IsTrue(edgeWithDifferentWeight.Equals(edge));
        }
        [Test]
        public void CompareEdges_DifferentStartingAndEndingNodes_SameWeights_ReturnsFalse()
        {
            Edge<String> differentEdge = Edge<String>.GetWeightedEdge("B", "C", 5);
            Assert.IsFalse(differentEdge.Equals(edge));
        }
    }
}
