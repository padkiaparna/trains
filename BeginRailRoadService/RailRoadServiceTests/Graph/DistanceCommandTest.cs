﻿using Moq;
using NUnit.Framework;
using RailRoadService;
using RailRoadService.Command;
using RailRoadService.CommuterEnquiry;
using System;
using System.Collections.Generic;

namespace RailRoadServiceTests.Graph
{
    [TestFixture]
    public class DistanceCommandTest
    {
        private Mock<ICommuterQuery> _commuter;
       
        private ICommand distanceCommand;

        [SetUp]
        public void Setup()
        {
            _commuter = new Mock<ICommuterQuery>();

            distanceCommand = new DistanceCommand("Distance: A-B-C");
        }

        [TearDown]
        public void Teardown()
        {
            _commuter.VerifyAll();
        }
        [Test]
        public void Execute_CallRouteDistance()
        {
            //arrange
            var intermediateNodes = new List<String>();
            intermediateNodes.Add("B");
            int routeDistance = 5;
            _commuter.Setup(c => c.RouteDistance("A", "C", intermediateNodes)).Returns(routeDistance);

            //act
            distanceCommand.Execute(_commuter.Object);

            //assert in teardown

        }

        [Test]
        public void Execute_ThrowsException()
        {
            //assert
            var intermediateNodes = new List<String>();
            intermediateNodes.Add("B");
            _commuter.Setup(c => c.RouteDistance("A", "C", intermediateNodes)).Throws(new NoSuchRouteException(""));

            //act
            distanceCommand.Execute(_commuter.Object);

            //assert in teardown
        }
    }
}
