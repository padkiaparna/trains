﻿using NUnit.Framework;
using RailRoadService;
using RailRoadService.CommuterEnquiry;
using RailRoadService.Graph;
using System;
using System.Collections.Generic;

namespace RailRoadServiceTests.CommuterEnquiry
{
    [TestFixture]
    class CommuterQueryTests
    {
        private ICommuterQuery _commuterQuery = new CommuterQuery(GraphBuilder.GetDefaultGraph());

        [Test]
        public void RouteDistance_distanceForABC_Returns9()
        {
            Assert.AreEqual(9,
                    _commuterQuery.RouteDistance("A", "C", GetIntermediateList("B")));
        }

        [Test]
        public void RouteDistance_distanceForAD_Returns5()
        {
            Assert.AreEqual(5, _commuterQuery.RouteDistance("A", "D", null));
        }

        [Test]
        public void RouteDistance_distanceForADC_Returns13()
        {
            Assert.AreEqual(13,
                    _commuterQuery.RouteDistance("A", "C", GetIntermediateList("D")));
        }

        [Test]
        public void RouteDistance_distanceForAEBCD_Returns22()
        {
            Assert.AreEqual(22,
                    _commuterQuery.RouteDistance("A", "D", GetIntermediateList("E", "B", "C")));
        }

        [Test]
        public void RouteDistance_distanceForAED_ThrowsNoSuchRouteException()
        {
            Assert.Throws<NoSuchRouteException>(() => _commuterQuery.RouteDistance("A", "D", GetIntermediateList("E")));
        }

        [Test]
        public void NumberOfPathsWithMaximumStops_BetweenCandCWithMax3Stops_Returns2()
        {
            Assert.AreEqual(2,
                    _commuterQuery.NumberOfPathsWithMaximumStops("C", "C", 3));
        }

        [Test]
        public void NumberOfPathsWithExactStops_BetweenAandCWith4Stops_Returns3()
        {
            Assert.AreEqual(3,
                    _commuterQuery.NumberOfPathsWithExactStops("A", "C", 4));
        }

        [Test]
        public void ShortestRoute_FromAtoC_Returns9()
        {
            Assert.AreEqual(9, _commuterQuery.ShortestRoute("A", "C"));
        }

        [Test]
        public void ShortestRoute_FromBtoB_Returns9()
        {
            Assert.AreEqual( 9, _commuterQuery.ShortestRoute("B", "B"));
        }

        [Test]
        public void NumberOfPathsWithMaximumWeight_MaxWeight30AndBetweenCandC_Returns7()
        {
            Assert.AreEqual(7,
                    _commuterQuery.NumberOfPathsWithMaximumWeight("C", "C", 30));
        }

        [Test]
        public void RouteDuration_ForABC_Returns11() //throws Exception
        {
            Assert.AreEqual(11,_commuterQuery.RouteDuration("A", "C", GetIntermediateList("B")));
        }

    private List<String> GetIntermediateList(params string[] nodes)
        {
            var intermediateList = new List<string>();
            foreach(var node in nodes)
            {
                intermediateList.Add(node);
            }
            return intermediateList;
        }
}
}
