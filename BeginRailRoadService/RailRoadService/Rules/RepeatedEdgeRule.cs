﻿using RailRoadService.Graph;

namespace RailRoadService.Rules
{
    public class RepeatedEdgeRule<T> : IRule<T>
    {
        public bool IsFulfilled(IPath<T> path)
        {
            return !path.HasRepeatedEdges();
        }
    }
}
