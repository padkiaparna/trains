﻿using RailRoadService.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailRoadService.Rules
{
    public interface IRule<T>
    {
        bool IsFulfilled(IPath<T> path);
    }
}
