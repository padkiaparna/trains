﻿namespace RailRoadService.Rules
{
    public class MaxWeightRule<T> : IRule<T>
    {
        private int _maxWeight;
        
        public MaxWeightRule(int maxWeight)
        {
            _maxWeight = maxWeight;
        }

        public bool IsFulfilled(Graph.IPath<T> path)
        {
            return path.GetPathTotalWeight() < _maxWeight;
        }
    }
}
