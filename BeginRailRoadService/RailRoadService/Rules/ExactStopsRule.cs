﻿using RailRoadService.Graph;

namespace RailRoadService.Rules
{
    public class ExactStopsRule<T> : IRule<T>
    {
        private int _numberOfStops;

        public ExactStopsRule(int numberOfStops)
        {
            _numberOfStops = numberOfStops;
        }

        public bool IsFulfilled(IPath<T> path)
        {
            return path.GetNumberOfStops() == _numberOfStops;
        }
    }
}
