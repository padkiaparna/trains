﻿using RailRoadService.Graph;

namespace RailRoadService.Rules
{
    public class MaxStopsRule<T> : IRule<T>
    {
        private int _maxStops;

        public MaxStopsRule(int maxStops)
        {
            _maxStops = maxStops;
        }

        public bool IsFulfilled(IPath<T> path)
        {
            return path.GetNumberOfStops() <= _maxStops;
        }
    }
}
