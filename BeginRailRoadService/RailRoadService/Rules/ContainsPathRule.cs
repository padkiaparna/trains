﻿using RailRoadService.Graph;

namespace RailRoadService.Rules
{
    public class ContainsPathRule<T> : IRule<T>
    {
        private IPath<T> _finalPath;

        public ContainsPathRule(IPath<T> path) {
            _finalPath = path;
        }
        public bool IsFulfilled(IPath<T> path)
        {
            return _finalPath.StartsWith(path);
        }
    }
}
