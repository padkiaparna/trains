﻿using System;
using System.Collections.Generic;
using System.Linq;
using RailRoadService.Graph;
using RailRoadService.Rules;

namespace RailRoadService.CommuterEnquiry
{
    public class CommuterQuery : ICommuterQuery
    {
        private IGraph<String> routes;

        public CommuterQuery(IGraph<String> routes)
        {
            this.routes = routes;
        }

        public IGraph<string> GetAllRoutes()
        {
            return routes;
        }

        public int NumberOfPathsWithExactStops(string startingCity, string destinationCity, int stops)
        {
            var paths = routes.GetAllPaths(startingCity, destinationCity, new MaxStopsRule<String>(stops));
            var exactPaths = new List<IPath<String>>();
            
            var exactStopsRule = new ExactStopsRule<String>(stops);
            foreach (var path in paths)
            {
                if (exactStopsRule.IsFulfilled(path))
                {
                    exactPaths.Add(path);
                }
            }
            return exactPaths.Count;
        }

        public int NumberOfPathsWithMaximumStops(string startingCity, string destinationCity, int stops)
        {
            return routes.GetAllPaths(startingCity, destinationCity, new MaxStopsRule<String>(stops)).Count;
        }

        public int NumberOfPathsWithMaximumWeight(string startingCity, string destinationCity, int weight)
        {
            return routes.GetAllPaths(startingCity, destinationCity, new MaxWeightRule<String>(weight)).Count;
        }

        public int RouteDistance(string startingCity, string destinationCity, List<string> intermediateCities)
        {
            var objectivePath = CreatePath(startingCity, destinationCity, intermediateCities);
            var finalPath = routes.GetAllPaths(startingCity, destinationCity, new ContainsPathRule<String>(
                    objectivePath));
            return finalPath[0].GetPathTotalWeight();
        }

        private IPath<String> CreatePath(string startingCity, string destinationCity, List<string> intermediateCities)
        {
            var resultingPath = GraphPath<String>.EmptyPath();
            String currentNode = startingCity;
            if (intermediateCities != null)
            {
                foreach(var city in intermediateCities)
                {
                    resultingPath.AddEdge(Edge<string>.GetWeightedEdge(currentNode, city, 0));
                    currentNode = city;
                }
            }
            resultingPath.AddEdge(Edge<string>.GetWeightedEdge(currentNode, destinationCity, 0));
            return resultingPath;
        }

        public int RouteDuration(string startingCity, string endCity, List<string> intermediateCities)
        {
            return RouteDistance(startingCity, endCity, intermediateCities) + 2 * intermediateCities.Count;
        }

        public int ShortestRoute(string startingCity, string destinationCity)
        {
            var allPaths = routes.GetAllPaths(startingCity, destinationCity,
                new RepeatedEdgeRule<String>());
            return allPaths.Min(a => a.GetPathTotalWeight());
        }

        IGraph<string> ICommuterQuery.GetAllRoutes()
        {
            return routes;
        }
    }

}
