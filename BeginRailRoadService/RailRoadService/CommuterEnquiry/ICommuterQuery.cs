﻿using RailRoadService.Graph;
using System;
using System.Collections.Generic;

namespace RailRoadService.CommuterEnquiry
{
    public interface ICommuterQuery
    {
        int NumberOfPathsWithExactStops(String startingCity, String destinationCity, int stops);
        int ShortestRoute(String startingCity, String destinationCity);

        IGraph<String> GetAllRoutes();

        int RouteDuration(String startingCity, String endCity, List<String> intermediateCities);
        int NumberOfPathsWithMaximumStops(String startingCity, String destinationCity, int stops);
        int NumberOfPathsWithMaximumWeight(String startingCity, String destinationCity, int weight);
        int RouteDistance(String startingCity, String endCity, List<String> intermediateCities);

    }
}
