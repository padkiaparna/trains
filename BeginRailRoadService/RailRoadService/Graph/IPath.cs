﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailRoadService.Graph
{
    public interface IPath<T> : IComparable<IPath<T>>
    {
        void AddEdge(IEdge<T> edge);
        int GetPathTotalWeight();
        int GetNumberOfStops();
        T GetLastNode();
        void RemoveLastEdge();
        IEnumerable<IEdge<T>> GetEdgeList();
        bool HasRepeatedEdges();
        bool StartsWith(IPath<T> otherPath);
    }
}
