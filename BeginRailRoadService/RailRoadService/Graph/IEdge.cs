﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailRoadService.Graph
{
    public interface IEdge<T>
    {
        T GetStartingNode();

        T GetEndingNode();

        int GetWeight();
    }
}
