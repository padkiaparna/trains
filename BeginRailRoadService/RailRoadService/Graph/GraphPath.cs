﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailRoadService.Graph
{
    public class GraphPath<T> : IPath<T>
    {
        private List<IEdge<T>> edgeList = new List<IEdge<T>>();
        private int totalWeight = 0;

        private GraphPath()
        {
        }

        private GraphPath(IPath<T> otherPath)
        {
            edgeList.AddRange(otherPath.GetEdgeList());
            totalWeight = otherPath.GetPathTotalWeight();
        }

        public static IPath<T> EmptyPath()
        {
            return new GraphPath<T>();
        }

        public static IPath<T> CopyPath(IPath<T> otherPath)
        {
            return new GraphPath<T>(otherPath);
        }

        public void AddEdge(IEdge<T> edge)
        {
            if (!edgeIsConsecutive(edge))
            {
                throw new ArgumentException("The edge " + edge + " is not consecutive to the existing path");
            }
            edgeList.Add(edge);
            totalWeight += edge.GetWeight();
        }

        private bool edgeIsConsecutive(IEdge<T> edge)
        {
            var lastNode = GetLastNode();
            if (lastNode != null && !lastNode.Equals(edge.GetStartingNode()))
            {
                return false;
            }
            return true;
        }

        public override String ToString()
        {
            return "GraphPath (" + totalWeight + ") [edgeList=" + edgeList + "]";
        }

        public int CompareTo(IPath<T> other)
        {
            return this.GetPathTotalWeight() - other.GetPathTotalWeight();
        }

        public IEnumerable<IEdge<T>> GetEdgeList()
        {
            return edgeList;
        }

        public T GetLastNode()
        {
            T node = default(T);
            if (edgeList.Count > 0)
            {
                node = edgeList[edgeList.Count - 1].GetEndingNode();
            }
            return node;
        }

        public int GetNumberOfStops()
        {
            return edgeList.Count;
        }

        public int GetPathTotalWeight()
        {
            return totalWeight;
        }

        public bool HasRepeatedEdges()
        {
            for (int i = 0; i < edgeList.Count; i++)
            {
                for (int j = i + 1; j < edgeList.Count; j++)
                {
                    if (edgeList[i].Equals(edgeList[j]))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void RemoveLastEdge()
        {
            if (edgeList.Count > 0)
            {
                var lastEdge = edgeList[edgeList.Count - 1];
                this.totalWeight -= lastEdge.GetWeight();
                edgeList.RemoveAt(edgeList.Count - 1);
            }
        }

        public bool StartsWith(IPath<T> otherPath)
        {
            var partialPath = otherPath.GetEdgeList().ToList();
            var completePath = GetEdgeList().ToList();
            for (int i = 0; i < partialPath.Count(); i++)
            {
                if (i >= completePath.Count() || !partialPath[i].Equals(completePath[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
