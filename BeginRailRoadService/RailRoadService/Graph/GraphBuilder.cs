﻿using System;

namespace RailRoadService.Graph
{
    public class GraphBuilder
    {
        public static IGraph<String> GetEmptyGraph()
        {
            return new DirectedGraph<String>();
        }

        public static IGraph<String> GetDefaultGraph()
        {
            IGraph<String> routeGraph = new DirectedGraph<String>();
            String nodeA = "A";
            String nodeB = "B";
            String nodeC = "C";
            String nodeD = "D";
            String nodeE = "E";

            routeGraph.AddNode(nodeA);
            routeGraph.AddNode(nodeB);
            routeGraph.AddNode(nodeC);
            routeGraph.AddNode(nodeD);
            routeGraph.AddNode(nodeE);

            routeGraph.AddEdge(nodeA, nodeB, 5);
            routeGraph.AddEdge(nodeB, nodeC, 4);
            routeGraph.AddEdge(nodeC, nodeD, 8);
            routeGraph.AddEdge(nodeD, nodeC, 8);
            routeGraph.AddEdge(nodeD, nodeE, 6);
            routeGraph.AddEdge(nodeA, nodeD, 5);
            routeGraph.AddEdge(nodeC, nodeE, 2);
            routeGraph.AddEdge(nodeE, nodeB, 3);
            routeGraph.AddEdge(nodeA, nodeE, 7);
            return routeGraph;
        }
    }
}
