﻿using System;
using System.Collections.Generic;
using RailRoadService.Rules;

namespace RailRoadService.Graph
{
    public class DirectedGraph<T> : IGraph<T>
    {
        private  Dictionary<T, HashSet<IEdge<T>>> edges = new Dictionary<T, HashSet<IEdge<T>>>();

        public bool AddEdge(T from, T to, int weight)
        {
            ThrowExceptionIfNodeIsAbsent(from);
            ThrowExceptionIfNodeIsAbsent(to);

            var newEdge = Edge<T>.GetWeightedEdge(from, to, weight);

            var sourceEdges = edges[from];
            
            if (sourceEdges.Contains(newEdge))
            {
                sourceEdges.Remove(newEdge);
            }
            return edges[from].Add(newEdge);
        }

        private void ThrowExceptionIfNodeIsAbsent(T node)
        {
            if (!edges.ContainsKey(node))
            {
                throw new ArgumentException("Vertex " + node.ToString() + " does not exist");
            }
        }

        public bool AddNode(T node)
        {
            ThrowExceptionIfNodeIsNull(node);
            if (!edges.ContainsKey(node))
            {
                edges[node] = new HashSet<IEdge<T>>();
                return true;
            }
            return false;
        }

        private void ThrowExceptionIfNodeIsNull(T node)
        {
            if (node == null)
            {
                throw new ArgumentException("Node can not be null");
            }
        }

        public HashSet<T> GetAllNodes()
        {
            var result = new HashSet<T>();
            foreach (var key in edges.Keys) {
                result.Add(key);
            }
            return result;
        }

        public List<IPath<T>> GetAllPaths(T startingNode, T endingNode, IRule<T> rule)
        {
            ThrowExceptionIfNodeIsAbsent(startingNode);
            ThrowExceptionIfNodeIsAbsent(endingNode);

            var paths = new List<IPath<T>>();
            foreach (var edge in edges[startingNode])
            {
                var path = GraphPath<T>.EmptyPath();
                path.AddEdge(edge);
                paths.AddRange(search(path, rule, endingNode));
            }

            if (paths.Count == 0)
            {
                throw new NoSuchRouteException("No route exists between " + startingNode + " and " + endingNode);
            }
            return paths;
        }

        private List<IPath<T>> search(IPath<T> path, IRule<T> rule, T endingNode)
        {
            var paths = new List<IPath<T>>();
            if (rule.IsFulfilled(path))
            {
                if (endIsReached(path, endingNode))
                {
                    paths.Add(GraphPath<T>.CopyPath(path));
                }
                foreach (var edge in edges[path.GetLastNode()])
                {
                    path.AddEdge(edge);
                    paths.AddRange(search(path, rule, endingNode));
                }

            }
            path.RemoveLastEdge();
            return paths;
        }

        private bool endIsReached(IPath<T> path, T endingNode)
        {
            return path.GetLastNode().Equals(endingNode);
        }

        public IEdge<T> GetEdge(T from, T to)
        {
            ThrowExceptionIfNodeIsAbsent(from);
            ThrowExceptionIfNodeIsAbsent(to);

            var startingNodeEdges = edges[from];
            foreach (var edge in startingNodeEdges)
            {
                if (edge.GetEndingNode().Equals(to))
                {
                    return edge;
                }
            }
            return null;
        }
    }
}
