﻿using RailRoadService.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailRoadService.Graph
{
    public interface IGraph<T>
    {
        bool AddEdge(T from, T to, int weight);
        bool AddNode(T node);
        IEdge<T> GetEdge(T from, T to);
        HashSet<T> GetAllNodes();
        List<IPath<T>> GetAllPaths(T startingNode, T endingNode, IRule<T> rule);

    }
}
