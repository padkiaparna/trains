﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailRoadService.Graph
{
    public class Edge<T> : IEdge<T>
    {
        private T _startingNode;
        private T _endingNode;
        private int _weight;

        private Edge(T startingNode, T endingNode, int weight)
        {
            _startingNode = startingNode;
            _endingNode = endingNode;
            _weight = weight;
        }
        public T GetEndingNode()
        {
            return _endingNode;
        }

        public T GetStartingNode()
        {
            return _startingNode;
        }

        public int GetWeight()
        {
            return _weight;
        }

        public static  Edge<T> GetWeightedEdge(T startingNode,  T endingNode,  int weight)
        {
            return new Edge<T>(startingNode, endingNode, weight);
        }

        public override bool Equals(Object obj)
        {
            var edge = obj as Edge<T>;
            if (edge == null) return false;
            Edge<T> other = (Edge<T>)obj;
            if (_endingNode == null)
            {
                if (other._endingNode != null)
                {
                    return false;
                }
            }
            else if (!_endingNode.Equals(other._endingNode))
            {
                return false;
            }
            if (_startingNode == null)
            {
                if (other._startingNode != null)
                {
                    return false;
                }
            }
            else if (!_startingNode.Equals(other._startingNode))
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + (_endingNode == null ? 0 : _endingNode.GetHashCode());
            result = prime * result + (_startingNode == null ? 0 : _startingNode.GetHashCode());
            return result;
        }

        
    }
}
