﻿using System;
using RailRoadService.CommuterEnquiry;

namespace RailRoadService.Command
{
    public class TripsCommand : AbstractCommand
    {
        const string MAX_STOPS = "MAX_STOPS";
        const string EXACT_STOPS = "EXACT_STOPS";
        const string MAX_DISTANCE = "MAX_DISTANCE";

        public TripsCommand(string commandLine) : base(commandLine)
        {
        }

        public override void Execute(ICommuterQuery commuter)
        {
            var routeLine = GetCommandLine().Substring(7);
            var commandParts = routeLine.Split(',');

            var filterCriteria = commandParts[0];
            var filterValue = Int32.Parse(commandParts[1]);
            var node = commandParts[2];
            var startNode = node[0].ToString();
            var endNode = node[2].ToString();

            int numberOfTrips = 0;

            try
            {
                if (filterCriteria.Equals(MAX_STOPS, StringComparison.OrdinalIgnoreCase))
                {
                    numberOfTrips = commuter.NumberOfPathsWithMaximumStops(startNode, endNode, filterValue);
                }
                else if (filterCriteria.Equals(EXACT_STOPS, StringComparison.OrdinalIgnoreCase))
                {
                    numberOfTrips = commuter.NumberOfPathsWithExactStops(startNode, endNode, filterValue);
                }
                else if (filterCriteria.Equals("MAX_DISTANCE", StringComparison.OrdinalIgnoreCase))
                {
                    numberOfTrips = commuter.NumberOfPathsWithMaximumWeight(startNode, endNode, filterValue);
                }
                Console.WriteLine(numberOfTrips);
            }
            catch (Exception) {
                Console.WriteLine(RouteCommand.NO_ROUTE_MSG);
            }
        }
    }
}
