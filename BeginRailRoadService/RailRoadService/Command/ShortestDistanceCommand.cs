﻿using System;
using RailRoadService.CommuterEnquiry;

namespace RailRoadService.Command
{
    public class ShortestDistanceCommand : AbstractCommand
    {
        public ShortestDistanceCommand(string commandLine) : base(commandLine)
        {
        }

        public override void Execute(ICommuterQuery commuter)
        {
            var routeLine = GetCommandLine().Substring(10);
            var from = routeLine[0].ToString();
            var to = routeLine[2].ToString();
            try
            {
                Console.WriteLine(commuter.ShortestRoute(from, to));
            }
            catch (NoSuchRouteException) {
                Console.WriteLine(RouteCommand.NO_ROUTE_MSG);
            }
            }
    }
}
