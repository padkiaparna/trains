﻿using RailRoadService.CommuterEnquiry;
using System;
using System.Collections.Generic;

namespace RailRoadService.Command
{
    public abstract class RouteCommand : AbstractCommand
    {

        internal const string NO_ROUTE_MSG = "NO SUCH ROUTE";

        public RouteCommand(String commandLine) : base(commandLine)
        {
        }

        protected abstract int CallCommuter(ICommuterQuery commuterQuery, String start, String end, List<String> intermediate);

        public override void Execute(ICommuterQuery commuterQuery)
        {
            var routeLine = GetCommandLine().Substring(10);
            var nodes = routeLine.Split('-');
            try
            {
                Console.WriteLine(CallCommuter(commuterQuery, nodes[0], nodes[nodes.Length - 1], GetIntermediateList(nodes)));
            }
            catch (NoSuchRouteException)
            {
                Console.WriteLine(NO_ROUTE_MSG);
            }
        }

        private List<String> GetIntermediateList(string[] nodes)
        {
            var intermediateList = new List<String>();
            for (int i = 1; i < nodes.Length - 1; i++)
            {
                intermediateList.Add(nodes[i]);
            }
            return intermediateList;
        }
    }
}
