﻿using RailRoadService.CommuterEnquiry;
using System;
using System.Collections.Generic;

namespace RailRoadService.Command
{
    
    public class DistanceCommand : RouteCommand
    {
        public DistanceCommand(string commandLine): base(commandLine)
        {
        }

        protected override int CallCommuter(ICommuterQuery commuterQuery, String start, String end, List<String> intermediate)
        {
            return commuterQuery.RouteDistance(start, end, intermediate);
        }
    }
}
