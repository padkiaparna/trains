﻿using System.Collections.Generic;
using RailRoadService.CommuterEnquiry;

namespace RailRoadService.Command
{
    public class DurationCommand : RouteCommand
    {
        public DurationCommand(string commandLine) : base(commandLine)
        {
        }

        protected override int CallCommuter(ICommuterQuery commuterQuery, string start, string end, List<string> intermediate)
        {
            return commuterQuery.RouteDuration(start, end, intermediate);
        }
    }
}
