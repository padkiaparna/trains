﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RailRoadService.Command
{
    public class CommandBuilder
    {
        private const string DISTANCE_PATTERN = @"DISTANCE:\s\b\w{1}\b(\-\w{1})*";
        private const string TRIPS_PATTERN = @"TRIPS:\s\b(MAX_STOPS|EXACT_STOPS|MAX_DISTANCE)\,\d+\,\w{1}\-\w{1}";
        private const string GRAPH_PATTERN = @"GRAPH:\s\b\w{2}\d+\b(\,\s\w{ 2}\d+)*";
        private const string SHORTEST_PATTERN = @"SHORTEST:\s\b\w{1}\-\w{1}";
        private const string DURATION_PATTERN = @"DURATION:\s\b\w{1}\b(\-\w{1})*";

        private string[] _listOfInput;

        public CommandBuilder(string[] listOfInput)
        {
            _listOfInput = listOfInput;
        }

        public List<ICommand> GetCommands()
        {
            var commands = new List<ICommand>();
            foreach (var input in _listOfInput)
            {
                ICommand command = BuildCommand(input.ToUpper());
                if (command != null)
                {
                    commands.Add(command);
                }
            }
            return commands;
        }

        private ICommand BuildCommand(string input)
        {
            ICommand builtCommand = null;
            if (Regex.Match(input, GRAPH_PATTERN).Success)
            {
                builtCommand = new BuildGraphCommand(input);
            }
            else if (Regex.Match(input, SHORTEST_PATTERN).Success)
            {
                builtCommand = new ShortestDistanceCommand(input);
            }
            else if (Regex.Match(input, TRIPS_PATTERN).Success)
            {
                builtCommand = new TripsCommand(input);
            }
            else if (Regex.Match(input, DISTANCE_PATTERN).Success)
            {
                builtCommand = new DistanceCommand(input);
            }
            else if (Regex.Match(input, DURATION_PATTERN).Success)
            {
                builtCommand = new DurationCommand(input);
            }
            else
            {
                Console.WriteLine("Line: " + input + " is invalid");
            }
            return builtCommand;
        }
    }
}
