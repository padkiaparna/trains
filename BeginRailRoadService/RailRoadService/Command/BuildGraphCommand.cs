﻿using System;
using RailRoadService.CommuterEnquiry;
using RailRoadService.Graph;

namespace RailRoadService.Command
{
    public class BuildGraphCommand : AbstractCommand
    {
        public BuildGraphCommand(string commandLine) : base(commandLine)
        {
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override void Execute(ICommuterQuery commuterQuery)
        {
            string nodesLine = GetCommandLine().Substring(7);
            string[] nodesInfo = nodesLine.Split(',');
            foreach (var nodeEdgeWeight in nodesInfo)
            {
                AddNodes(commuterQuery.GetAllRoutes(), nodeEdgeWeight);
            }

        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        private void AddNodes(IGraph<String> graph,string nodeEdgeWeight)
        {
            var _nodeEdgeWeight = nodeEdgeWeight.Trim();
            var from = _nodeEdgeWeight[0].ToString();
            var to = _nodeEdgeWeight[1].ToString();
            var weight = _nodeEdgeWeight.Substring(2);
            graph.AddNode(from);
            graph.AddNode(to);
            graph.AddEdge(from.ToString(), to, Int32.Parse(weight));
        }
    }
}
