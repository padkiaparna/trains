﻿using System;
using RailRoadService.CommuterEnquiry;

namespace RailRoadService.Command
{
    public abstract class AbstractCommand : ICommand
    {
        private String commandLine;

        public AbstractCommand(string commandLine)
        {
            this.commandLine = commandLine;
        }

        protected String GetCommandLine()
        {
            return commandLine;
        }

        
        public override int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + (commandLine == null ? 0 : commandLine.GetHashCode());
            return result;
        }

        
        public override bool Equals(Object obj)
        {
            var edge = obj as AbstractCommand;
            if (edge == null) return false;
            AbstractCommand other = (AbstractCommand)obj;
            if (commandLine == null)
            {
                if (other.commandLine != null)
                {
                    return false;
                }
            }
            else if (!commandLine.Equals(other.commandLine))
            {
                return false;
            }
            return true;
        }

        public abstract void Execute(ICommuterQuery commuter);
    }
}
