﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RailRoadService
{
    public class NoSuchRouteException : Exception
    {
        public NoSuchRouteException(String message) : base(message)
        {
        }
    }
}
