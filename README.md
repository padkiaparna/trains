Problem Solved: Trains
---

## Stack used 

- Language : C#
- Framework: .Net 4.6.1
- Testing Framework: Nunit



## Assumptions

Input will be in following format:

First line will be the graph to be built -- Graph: AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7

Followed by commuter queries :

1. The distance of the route A-B-C.   --  Distance: A-B-C
2. The number of trips starting at C and ending at C with a maximum of 3 stops.   --  Trips: MAX_STOPS,3,C-C
3. The length of the shortest route (in terms of distance to travel) from A to C. -- Shortest: B-B
4. The number of different routes from C to C with a distance of less than 30.    -- Trips: MAX_DISTANCE,30,C-C
5. The number of trips starting at A and ending at C with exactly 4 stops.        -- Trips: EXACT_STOPS,4,A-C


---

## Run the application

Solution is implemented as a console application. It consists of 3 projects:

1. BeginRailRoadService 
   Driver of the application. Set this project as a startup project to run the app. On console, user is prompted to enter path to the input file. 
   If given path is invalid, app uses the input file that resides in this project.
   
2. RailRoadService
   This project consists the implementation of the solution.
   
3. RailRoadServiceTests
   This project consists unit tests for classes in RailRoadService project.
   
To run the app, open BeginRailRoadService.sln in visual studio. Set BeginRailRoadService as start up project.

Type 'exit' to quit the app.


## Design
- Builder design pattern is used to build the appropriate commands based on the input. 
- I tried to follow SOLID design principles, by dividing requirements into separate classes, usage of interfaces for
the flexibility to introduce new features.
- Abstract classes are used to abstract out common functionalities in all their concrete classes. Keeping the shared functionality a responsibility of the abstract class.

